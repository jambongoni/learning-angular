import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.scss']
})
export class CardContainerComponent implements OnInit {
  containerItems: string[];
  constructor() {
  }
  
  ngOnInit() {
    this.containerItems = ['one', 'two', 'three', 'four'];
  }

  hiThereFromContainer(hello: string) {
    console.log(`from container: ${hello}`);
    // console.log('from container: ' + hello);
  }



}
