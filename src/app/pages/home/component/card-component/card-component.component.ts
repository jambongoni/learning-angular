import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-card-component',
  templateUrl: './card-component.component.html',
  styleUrls: ['./card-component.component.scss']
})
export class CardComponentComponent implements OnInit {
  // getting information from a container/smart component
  @Input() componentItem: string[];

  // for pushing information upwards to a container/smart component
  @Output() hello: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.componentItem);

    this.greeting();
  }

  greeting() {
    this.hello.emit('hello world');
  }


}
